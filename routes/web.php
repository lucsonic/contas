<?php

use App\Http\Controllers\CartaoController;
use App\Http\Controllers\ContaspController;
use App\Http\Controllers\DespesaController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// rotas de login
Route::view('/', 'login')->name('login');
Route::post('/autenticar', [LoginController::class, 'autenticar'])->name('autenticar');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
Route::get('/troca-senha', [LoginController::class, 'trocaSenha'])->name('trocaSenha');
Route::post('/trocar-senha', [LoginController::class, 'trocarSenha'])->name('trocarSenha');
Route::get('/novo-usuario', [LoginController::class, 'novoUsuario'])->name('novoUsuario');
Route::post('/cadastrar-usuario', [LoginController::class, 'cadastrarUsuario'])->name('cadastrarUsuario');

// rota para home
Route::view('/home', 'home')->name('home');

// rotas para cartoes
Route::get('/cartoes', [CartaoController::class, 'getCartoes'])->name('cartoes');

// rotas de cartão
Route::get('/despesa', [DespesaController::class, 'getDespesas'])->name('despesa');
Route::get('/cadastro-despesa', [DespesaController::class, 'cadastroDespesa'])->name('cadastroDespesa');
Route::post('/cadastrar-despesa', [DespesaController::class, 'cadastrarDespesa'])->name('cadastrarDespesa');
Route::get('/editar-despesa/{codDespesa}', [DespesaController::class, 'editarDespesa'])->name('editarDespesa');
Route::post('/alterar-despesa', [DespesaController::class, 'alterarDespesa'])->name('alterarDespesa');
Route::get('/excluir-despesa/{codDespesa}', [DespesaController::class, 'excluirDespesa'])->name('excluirDespesa');
Route::post('/pesquisa-periodo', [DespesaController::class, 'pesquisaPeriodo'])->name('pesquisaPeriodo');

// rotas de contas a pagar
Route::get('/contas', [ContaspController::class, 'getContasp'])->name('contas');
Route::get('/cadastro-conta', [ContaspController::class, 'cadastroContap'])->name('cadastroContap');
Route::post('/cadastrar-conta', [ContaspController::class, 'cadastrarContap'])->name('cadastrarContap');
Route::get('/editar-conta/{codContasp}', [ContaspController::class, 'editarContap'])->name('editarContap');
Route::post('/alterar-conta', [ContaspController::class, 'alterarContap'])->name('alterarContap');
Route::get('/excluir-conta/{codContasp}', [ContaspController::class, 'excluirContap'])->name('excluirContap');
Route::get('/pagar-conta/{codContasp}', [ContaspController::class, 'pagarContap'])->name('pagarContap');
Route::post('/pesquisa-contasmes', [ContaspController::class, 'pesquisaContasMes'])->name('pesquisaContasMes');