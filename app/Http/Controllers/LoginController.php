<?php

namespace App\Http\Controllers;

use App\Models\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function autenticar(Request $request)
    {
        $usuario = Login::where(Login::DSC_EMAIL, '=', $request->dsc_email)->first();

        if ($usuario) {
            if (password_verify($request->dsc_senha, $usuario->dsc_senha) && $usuario->flg_ativo === '1') {
                $nome = explode(' ', $usuario->nom_usuario);
                session(['perfil' => $usuario->cod_perfil]);
                session(['nome' => $nome[0]]);
                session(['nome_completo' => $usuario->nom_usuario]);
                session(['cod_usuario' => $usuario->cod_usuario]);
                return redirect(route('home'));
            }

            if (!password_verify($request->dsc_senha, $usuario->dsc_senha)) {
                return redirect(route('login'))->with("mensagem", "Senha incorreta!");
            }
        }

        return redirect(route('login'))->with("mensagem", "Usuário não encontrado");
    }

    public function logout()
    {
        Session::flush();
        Auth::logout();
        return redirect(route('login'));
    }

    public function trocaSenha()
    {
        return view('troca-senha');
    }

    public function trocarSenha(Request $request)
    {
        $login = Login::find($request->cod_usuario);
        try {
            DB::beginTransaction();

            $login->update([
                'dsc_senha' => password_hash($request->dsc_senha, PASSWORD_DEFAULT)
            ]);

            DB::commit();
            return redirect(route('login'));
        } catch (\Exception $e) {
            return back()->withErrors($e)->withInput();
        }
    }

    public function novoUsuario()
    {
        return view('novo-usuario');
    }

    public function cadastrarUsuario(Request $request)
    {
        try {
            DB::beginTransaction();

            Login::create([
                Login::NOM_USUARIO => $request->nom_usuario,
                Login::DSC_EMAIL => $request->dsc_email,
                Login::DSC_SENHA => password_hash($request->dsc_senha, PASSWORD_DEFAULT),
                Login::FLG_ATIVO => '1',
                Login::COD_PERFIL => 2
            ]);

            DB::commit();
            return redirect(route('home'));
        } catch (\Exception $e) {
            return back()->withErrors($e)->withInput();
        }
    }
}
