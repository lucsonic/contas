<?php

namespace App\Http\Controllers;

use App\Models\Contasp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContaspController extends Controller
{
    private $contaspModel;

    public function __construct()
    {
        $this->contaspModel = new Contasp();
    }

    public function getContasp()
    {
        if (session('perfil')) {
            $month = date('m');
            $year = $month === '12' ? date("Y") +1 : date("Y");

            $consulta = $this->contaspModel->query();
            $consulta->select(Contasp::TABLE . '.' . '*');
            $consulta->where(Contasp::TABLE . '.' . Contasp::COD_USUARIO, '=', session('cod_usuario'));
            $consulta->where(Contasp::TABLE . '.' . Contasp::NOM_MES, '=', getProximoMes($month));
            $consulta->where(Contasp::TABLE . '.' . Contasp::NUM_ANO, '=', $year);
            $consulta->orderBy(Contasp::TABLE . '.' . Contasp::DSC_CONTAP, 'ASC');

            $contas = $consulta->get();
            return view('contas', compact('contas'));
        } else {
            return redirect(route('login'));
        }
    }

    public function cadastroContap()
    {
        return view('cadastrar-conta');
    }

    public function editarContap($codContasp)
    {
        $conta = Contasp::find($codContasp);
        return view('cadastrar-conta', compact('conta'));
    }

    public function cadastrarContap(Request $request)
    {
        try {
            DB::beginTransaction();

            Contasp::create([
                Contasp::COD_USUARIO => session('cod_usuario'),
                Contasp::DSC_CONTAP => $request->dsc_contap,
                Contasp::NOM_MES => $request->nom_mes,
                Contasp::NUM_ANO => $request->num_ano,
                Contasp::DAT_CADASTRO => Carbon::now(),
                Contasp::VLR_CONTAP => valorDB($request->vlr_contap),
                Contasp::FLG_PAGO => '0'
            ]);

            DB::commit();
            return redirect(route('contas'))->with("mensagem", 'Conta a pagar cadastrada com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors($e)->withInput();
        }
    }

    public function excluirContap(int $codContasp)
    {
        try {
            DB::beginTransaction();
            Contasp::where('cod_contasp', $codContasp)->delete();
            DB::commit();
            return redirect(route('contas'))->with("mensagem", 'Conta a pagar excluída com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors($e)->withInput();
        }
    }

    public function alterarContap(Request $request)
    {
        $conta = Contasp::find($request->cod_contasp);
        try {
            DB::beginTransaction();

            $conta->update([
                Contasp::COD_USUARIO => session('cod_usuario'),
                Contasp::DSC_CONTAP => $request->dsc_contap,
                Contasp::NOM_MES => $request->nom_mes,
                Contasp::NUM_ANO => $request->num_ano,
                Contasp::DAT_CADASTRO => Carbon::now(),
                Contasp::VLR_CONTAP => valorDB($request->vlr_contap)
            ]);

            DB::commit();
            return redirect(route('contas'))->with("mensagem", 'Conta a pagar alterada com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors($e)->withInput();
        }
    }

    public function pagarContap(int $codContasp)
    {
        $conta = Contasp::find($codContasp);
        try {
            DB::beginTransaction();

            $conta->update([
                Contasp::FLG_PAGO => $conta->flg_pago === '0' ? '1' : '0',
                Contasp::DAT_CADASTRO => Carbon::now()
            ]);

            DB::commit();
            return redirect(route('contas'))->with("mensagem", 'Ação concluída com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors($e)->withInput();
        }
    }

    public function pesquisaContasMes(Request $request)
    {
        $consulta = $this->contaspModel->query();
        $consulta->select(Contasp::TABLE . '.' . '*');
        $consulta->where(Contasp::TABLE . '.' . Contasp::NOM_MES, '=', $request->mes);
        $consulta->where(Contasp::TABLE . '.' . Contasp::NUM_ANO, '=', $request->ano);
        $consulta->where(Contasp::TABLE . '.' . Contasp::COD_USUARIO, '=', session('cod_usuario'));
        $consulta->orderBy(Contasp::TABLE . '.' . Contasp::DSC_CONTAP, 'ASC');

        $contas = $consulta->get();
        $pesquisa = 'Contas a pagar de ' . $request->mes . ' de ' . $request->ano;
        return view('contas', compact('contas', 'pesquisa'));
    }
}
