<?php

namespace App\Http\Controllers;

use App\Models\Cartao;

class CartaoController extends Controller
{
    private $cartaoModel;

    public function __construct()
    {
        $this->cartaoModel = new Cartao();
    }

    public function getCartoes()
    {
        return $this->cartaoModel->all();
    }
}
