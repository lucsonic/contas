<?php

namespace App\Http\Controllers;

use App\Models\Cartao;
use App\Models\Despesa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DespesaController extends Controller
{
    private $despesaModel;
    private $cartoesModel;

    public function __construct()
    {
        $this->despesaModel = new Despesa();
        $this->cartoesModel = new Cartao();
    }

    public function getDespesas()
    {
        if (session('perfil')) {
            $P_Dia = date("Y-m-01");
            $U_Dia = date("Y-m-t");

            $consulta = $this->despesaModel->query();
            $consulta->select(Despesa::TABLE . '.' . '*', Cartao::TABLE . '.' . Cartao::DSC_BANDEIRA);
            $consulta->join(Cartao::TABLE, Despesa::TABLE . '.' . Despesa::COD_CARTAO, '=', Cartao::TABLE . '.' . Cartao::COD_CARTAO);
            $consulta->where(Despesa::TABLE . '.' . Despesa::DAT_DESPESA, '>=', $P_Dia);
            $consulta->where(Despesa::TABLE . '.' . Despesa::DAT_DESPESA, '<=', $U_Dia);
            $consulta->where(Despesa::TABLE . '.' . Despesa::COD_USUARIO, '=', session('cod_usuario'));
            $consulta->orderBy(Despesa::TABLE . '.' . Despesa::DAT_DESPESA, 'ASC');

            $despesas = $consulta->get();
            return view('despesa', compact('despesas'));
        } else {
            return redirect(route('login'));
        }
    }

    public function editarDespesa($codDespesa)
    {
        $desp = Despesa::find($codDespesa);

        $consulta = $this->despesaModel->query();
        $consulta->select(Despesa::TABLE . '.' . '*', Cartao::TABLE . '.' . Cartao::DSC_BANDEIRA);
        $consulta->join(Cartao::TABLE, Despesa::TABLE . '.' . Despesa::COD_CARTAO, '=', Cartao::TABLE . '.' . Cartao::COD_CARTAO);
        $consulta->where(Despesa::TABLE . '.' . Despesa::COD_DESPESA, '=', $codDespesa);
        $despesa = $consulta->first();

        $cartoes = $this->cartoesModel->all();
        return view('cadastrar-despesa', compact('despesa', 'cartoes', 'desp'));
    }

    public function alterarDespesa(Request $request)
    {
        $consulta = $this->despesaModel->query();
        $consulta->select(Despesa::TABLE . '.' . '*');
        $consulta->where(Despesa::TABLE . '.' . Despesa::DSC_DESPESA, '=', $request->descricao);
        $consulta->where(Despesa::TABLE . '.' . Despesa::DSC_ESTABELECIMENTO, '=', $request->estabelecimento);
        $consulta->where(Despesa::TABLE . '.' . Despesa::VLR_DESPESA, '=', $request->valor);
        $consulta->orderBy(Despesa::TABLE . '.' . Despesa::COD_DESPESA, 'ASC');

        $result = $consulta->get();

        try {
            DB::beginTransaction();
            foreach ($result as $key => $value) {
                $desp = Despesa::find($value->cod_despesa);
                $dataParcela = adicionaTrintaDias(dateDB($request->dat_despesa), $key + 1);

                $desp->update([
                    Despesa::COD_CARTAO => $request->cod_cartao,
                    Despesa::COD_USUARIO => session('cod_usuario'),
                    Despesa::DAT_DESPESA => dateDB($dataParcela),
                    Despesa::DSC_DESPESA => $request->dsc_despesa,
                    Despesa::NUM_PARCELA => $key + 1,
                    Despesa::TOT_PARCELAS => $request->tot_parcelas,
                    Despesa::DSC_ESTABELECIMENTO => $request->dsc_estabelecimento,
                    Despesa::VLR_DESPESA => valorDB($request->vlr_despesa)
                ]);
            }
            DB::commit();
            // return redirect(route('despesa'))->with('success', 'Despesa alterada com sucesso!');
            return redirect(route('despesa'))->with("mensagem", 'Despesa alterada com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors($e)->withInput();
        }
    }

    public function cadastroDespesa()
    {
        $cartoes = $this->cartoesModel->all();
        return view('cadastrar-despesa', compact('cartoes'));
    }

    public function cadastrarDespesa(Request $request)
    {
        $parcelas = intval($request->tot_parcelas);
        $valor = valorDB($request->vlr_despesa);
        $parcela = $valor / $parcelas;

        try {
            DB::beginTransaction();

            for ($i = 1; $i <= intval($parcelas); $i++) {
                $dataParcela = adicionaTrintaDias(dateDB($request->dat_despesa), $i);
                Despesa::create([
                    Despesa::COD_CARTAO => $request->cod_cartao,
                    Despesa::COD_USUARIO => session('cod_usuario'),
                    Despesa::DAT_DESPESA => dateDB($dataParcela),
                    Despesa::DSC_DESPESA => $request->dsc_despesa,
                    Despesa::NUM_PARCELA => $i,
                    Despesa::TOT_PARCELAS => $parcelas,
                    Despesa::DSC_ESTABELECIMENTO => $request->dsc_estabelecimento,
                    Despesa::VLR_DESPESA => $parcela
                ]);
            }

            DB::commit();
            return redirect(route('despesa'))->with("mensagem", 'Despesa cadastrada com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors($e)->withInput();
        }
    }

    public function excluirDespesa($codDespesa)
    {
        $despesa = Despesa::find($codDespesa);

        $consulta = $this->despesaModel->query();
        $consulta->select(Despesa::TABLE . '.' . '*');
        $consulta->where(Despesa::TABLE . '.' . Despesa::DSC_DESPESA, '=', $despesa->dsc_despesa);
        $consulta->where(Despesa::TABLE . '.' . Despesa::DSC_ESTABELECIMENTO, '=', $despesa->dsc_estabelecimento);
        $consulta->where(Despesa::TABLE . '.' . Despesa::VLR_DESPESA, '=', $despesa->vlr_despesa);
        $consulta->orderBy(Despesa::TABLE . '.' . Despesa::COD_DESPESA, 'ASC');

        $result = $consulta->get();

        try {
            DB::beginTransaction();

            foreach ($result as $key => $value) {
                Despesa::where('cod_despesa', $value->cod_despesa)->delete();
            }

            DB::commit();
            return redirect(route('despesa'))->with("mensagem", 'Despesa excluída com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors($e)->withInput();
        }
    }

    public function pesquisaPeriodo(Request $request)
    {
        $consulta = $this->despesaModel->query();
        $consulta->select(Despesa::TABLE . '.' . '*', Cartao::TABLE . '.' . Cartao::DSC_BANDEIRA);
        $consulta->join(Cartao::TABLE, Despesa::TABLE . '.' . Despesa::COD_CARTAO, '=', Cartao::TABLE . '.' . Cartao::COD_CARTAO);
        $consulta->where(Despesa::TABLE . '.' . Despesa::DAT_DESPESA, '>=', dateDB($request->dat_inicial));
        $consulta->where(Despesa::TABLE . '.' . Despesa::DAT_DESPESA, '<=', dateDB($request->dat_final));
        $consulta->where(Despesa::TABLE . '.' . Despesa::COD_USUARIO, '=', session('cod_usuario'));
        $consulta->orderBy(Despesa::TABLE . '.' . Despesa::DAT_DESPESA, 'ASC');

        $despesas = $consulta->get();
        $pesquisa = 'Despesas de ' . $request->dat_inicial . ' a ' . $request->dat_final;
        return view('despesa', compact('despesas', 'pesquisa'));
    }
}
