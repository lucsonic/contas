<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cartao extends Model
{
    use HasFactory;

    const TABLE = 'tab_cartao';
    const COD_CARTAO = 'cod_cartao';
    const DSC_BANDEIRA = 'dsc_bandeira';
    const FLG_ATIVO = 'flg_ativo';
    const CREATED_AT = null;
    const UPDATED_AT = null;
    const DELETED_AT = null;

    //Constantes
    const CARTAO_ATIVO = '1';
    const CARTAO_INATIVO = '0';

    protected $table = self::TABLE;
    public $primaryKey = self::COD_CARTAO;

    public $fillable = [
        self::COD_CARTAO,
        self::DSC_BANDEIRA,
        self::FLG_ATIVO
    ];

    //Carbon Dates
    protected $dates = [
        self::DELETED_AT,
        self::CREATED_AT,
        self::UPDATED_AT
    ];
}
