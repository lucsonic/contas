<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Login extends Authenticatable
{
    use HasFactory, Notifiable;

    const TABLE = 'tab_usuario';
    const COD_USUARIO = 'cod_usuario';
    const NOM_USUARIO = 'nom_usuario';
    const DSC_EMAIL = 'dsc_email';
    const DSC_SENHA = 'dsc_senha';
    const FLG_ATIVO = 'flg_ativo';
    const COD_PERFIL = 'cod_perfil';
    const CREATED_AT = null;
    const UPDATED_AT = null;
    const DELETED_AT = null;

    protected $table = self::TABLE;
    public $primaryKey = self::COD_USUARIO;

    public $fillable = [
        self::COD_USUARIO,
        self::NOM_USUARIO,
        self::DSC_EMAIL,
        self::DSC_SENHA,
        self::FLG_ATIVO,
        self::COD_PERFIL
    ];

    //Carbon Dates
    protected $dates = [
        self::DELETED_AT,
        self::CREATED_AT,
        self::UPDATED_AT
    ];
}
