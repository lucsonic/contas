<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Despesa extends Model
{
    use HasFactory;

    const TABLE = 'tab_despesa';
    const COD_DESPESA = 'cod_despesa';
    const COD_CARTAO = 'cod_cartao';
    const COD_USUARIO = 'cod_usuario';
    const DAT_DESPESA = 'dat_despesa';
    const DSC_DESPESA = 'dsc_despesa';
    const NUM_PARCELA = 'num_parcela';
    const TOT_PARCELAS = 'tot_parcelas';
    const DSC_ESTABELECIMENTO = 'dsc_estabelecimento';
    const VLR_DESPESA = 'vlr_despesa';
    const CREATED_AT = null;
    const UPDATED_AT = null;
    const DELETED_AT = null;

    protected $table = self::TABLE;
    public $primaryKey = self::COD_DESPESA;

    public $fillable = [
        self::COD_DESPESA,
        self::COD_CARTAO,
        self::COD_USUARIO,
        self::DAT_DESPESA,
        self::DSC_DESPESA,
        self::NUM_PARCELA,
        self::TOT_PARCELAS,
        self::DSC_ESTABELECIMENTO,
        self::VLR_DESPESA
    ];

    //Carbon Dates
    protected $dates = [
        self::DELETED_AT,
        self::CREATED_AT,
        self::UPDATED_AT
    ];

    public function cartao()
    {
        return $this->hasOne(Cartao::class, Cartao::COD_CARTAO, self::COD_CARTAO);
    }
}
