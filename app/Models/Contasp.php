<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contasp extends Model
{
    use HasFactory;

    const TABLE = 'tab_contasp';
    const COD_CONTASP = 'cod_contasp';
    const COD_USUARIO = 'cod_usuario';
    const DSC_CONTAP = 'dsc_contap';
    const NOM_MES = 'nom_mes';
    const NUM_ANO = 'num_ano';
    const DAT_CADASTRO = 'dat_cadastro';
    const VLR_CONTAP = 'vlr_contap';
    const FLG_PAGO = 'flg_pago';
    const CREATED_AT = null;
    const UPDATED_AT = null;
    const DELETED_AT = null;


    protected $table = self::TABLE;
    public $primaryKey = self::COD_CONTASP;

    public $fillable = [
        self::COD_CONTASP,
        self::COD_USUARIO,
        self::DSC_CONTAP,
        self::NOM_MES,
        self::NUM_ANO,
        self::DAT_CADASTRO,
        self::VLR_CONTAP,
        self::FLG_PAGO
    ];

    //Carbon Dates
    protected $dates = [
        self::DELETED_AT,
        self::CREATED_AT,
        self::UPDATED_AT
    ];
}
