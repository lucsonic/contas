<?php

use Carbon\Carbon;

function getVersao()
{
    $versao = "";
    exec("cd " . $_SERVER['DOCUMENT_ROOT'] . " && git rev-parse --abbrev-ref HEAD", $branchName);
    $branchName = current($branchName);
    if ($branchName == "master") {
        $versao = "Master";
    } elseif ($branchName == "HEAD") {
        exec("cd " . $_SERVER['DOCUMENT_ROOT'] . " && git describe --tags --abbrev=0", $tagName);
        $versao = array_pop($tagName);
    } else {
        $versao = $branchName;
    }
    return $versao;
}

function tirarAcentos($string)
{
    return preg_replace(array(
        "/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/",
        "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"
    ), explode(" ", "a A e E i I o O u U n N"), $string);
}

function trataNomeImage($img)
{
    return str_replace(" ", "_", $img);
}

function dataFrontPHP($dta)
{
    return date('d/m/Y', strtotime($dta));
}

function dateDB($data)
{
    $dataFormat = null;
    if (isset($data) && $data) :
        $data  = Carbon::createFromFormat('d/m/Y', $data);
        $dataFormat  = $data->format('Y-m-d');
    endif;

    return $dataFormat;
}

function valorDB($valor)
{
    $verificaPonto = ".";
    if (strpos("[" . $valor . "]", "$verificaPonto")) {
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);
    } else {
        $valor = str_replace(',', '.', $valor);
    }

    return $valor;
}

function valorFrontend($valor)
{
    return number_format($valor, 2, ",", ".");
}

function getProximoMes($n) {
    switch ($n) {
        case 1:
            return 'Fevereiro';
            break;
        case 2:
            return 'Março';
            break;
        case 3:
            return 'Abril';
            break;
        case 4:
            return 'Maio';
            break;
        case 5:
            return 'Junho';
            break;
        case 6:
            return 'Julho';
            break;
        case 7:
            return 'Agosto';
            break;
        case 8:
            return 'Setembro';
            break;
        case 9:
            return 'Outubro';
            break;
        case 10:
            return 'Novembro';
            break;
        case 11:
            return 'Dezembro';
            break;
        case 12:
            return 'Janeiro';
            break;

        default:
            # code...
            break;
    }
}

function adicionaTrintaDias($dt, $n)
{
    $qtdDias = 0;
    switch ($n) {
        case '2':
            $qtdDias = 30;
            break;
        case '3':
            $qtdDias = 60;
            break;
        case '4':
            $qtdDias = 90;
            break;
        case '5':
            $qtdDias = 120;
            break;
        case '6':
            $qtdDias = 150;
            break;
        case '7':
            $qtdDias = 180;
            break;
        case '8':
            $qtdDias = 210;
            break;
        case '9':
            $qtdDias = 240;
            break;
        case '10':
            $qtdDias = 270;
            break;
        case '11':
            $qtdDias = 300;
            break;
        case '12':
            $qtdDias = 330;
            break;
        case '13':
            $qtdDias = 360;
            break;
        case '14':
            $qtdDias = 390;
            break;
        case '15':
            $qtdDias = 420;
            break;
        case '16':
            $qtdDias = 450;
            break;
        case '17':
            $qtdDias = 480;
            break;
        case '18':
            $qtdDias = 510;
            break;
        case '19':
            $qtdDias = 540;
            break;
        case '20':
            $qtdDias = 570;
            break;
        case '21':
            $qtdDias = 600;
            break;
        case '22':
            $qtdDias = 630;
            break;
        case '23':
            $qtdDias = 660;
            break;
        case '24':
            $qtdDias = 690;
            break;

        default:
            # code...
            break;
    }

    return date('d/m/Y', strtotime("+{$qtdDias} days", strtotime($dt)));
}
