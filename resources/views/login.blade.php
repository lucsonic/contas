@extends('templates.template_login.main')
@section('title', 'Login')

@section('content')
<div id="appLogin" class="h-100 d-flex align-items-center justify-content-center">
  <form action="/autenticar" method="post">
    @csrf
    <div class="card" style="container col-md-6 width: 30rem; margin-top: 50%; margin-bottom: 50%; background-color: #F8F8FF; box-shadow: 2px 2px 2px 1px; border: 1px solid navy;">
      <div class="card-body">
        <div style="margin-bottom: 20px; text-align: center; display: flex;" class="text-secondary">
          <table>
            <tr>
              <td>
                <img src="{{asset('assets/img/logo.png')}}" width="80" height="80"/>
              </td>
              <td>
                <div style="margin-left: 20px; text-align: left;">
                  <h3 class="card-title">Login</h3>
                  <h6>Informe suas credenciais</h6>
                </div>
              </td>
            </tr>
          </table>
        </div>
        <div class="rounded" style="border: solid 1px #c2c2c2; padding: 15px;">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="dsc_email"><b>Email</b>&nbsp;</span>
            </div>
            <input type="text" class="form-control" aria-label="Email" value="" aria-describedby="dsc_email" id="dsc_email" name="dsc_email" required />
          </div>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text" id="dsc_senha"><b>Senha</b></span>
            </div>
            <input type="password" class="form-control" aria-label="Email" value="" aria-describedby="dsc_senha" id="dsc_senha" name="dsc_senha" required />
          </div>
        </div>
        @if(session('mensagem'))
        <div style="padding-top: 10px;">
          <div class="alert alert-danger" id="danger-alert">
            <strong>Atenção! </strong> {{session('mensagem')}}
          </div>
        </div>
        @endif
        <div style="padding-top: 15px; text-align: right;">
          <button type="submit" class="btn btn-primary">&nbsp;&nbsp;<i class="fas fa-user-check"></i>&nbsp;&nbsp;Entrar&nbsp;&nbsp;</button>
        </div>
        <div style="width: 100%; display: flex; padding-top: 15px;">
          <div class="fw-bold" style="float: left; width: 65%; color: gray;">Controle Financeiro</div>
          <div style="float: right; width: 35%; text-align: right;"><?= 'v. ' . substr(getVersao(), 1)?></span></div>
        </div>
      </div>
    </div>
  </form>
</div>
    <script>
$(document).ready(function() {
    $("#danger-alert").fadeTo(2000, 500).slideUp(500, function() {
        $("#danger-alert").slideUp(500);
    });
});
</script>
@endsection
