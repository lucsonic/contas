@extends('templates.template_padrao.master')
@section('title', 'Troca senha')

@section('content')
    <div style="margin-bottom: 20px; margin-top: 10px; font-weight: bold;">
        <h4><i class="fas fa-key"></i>&nbsp;Alteração de Senha</h4>
    </div>
    <h5 style="color: navy; font-weight: bold;">{{session('nome_completo')}}</h5>
    <form action="{{ route('trocarSenha') }}" method="post">
    @csrf
    <div class="card" id="divTrocaSenha" style="background-color: #f1f1f1;">
        <div class="card-body">
            <div class="row" style="margin-bottom: 15px;">
                <input type="hidden" name="cod_usuario" value="{{ session('cod_usuario') }}">
                <div class="form-group col-sm-12">
                    <label for="dsc_senha" class="fw-bold">INFORME A NOVA SENHA</label>
                    <input type="password" value="" required class="form-control" id="dsc_senha" name="dsc_senha" placeholder="Nova senha">
                </div>
            </div>
        </div>
    </div>
    <div style="text-align: right; margin-top: 20px;">
        <a type="buttom" href="{{ route('home') }}" class="btn btn-secondary"><i class="fa fa-ban"></i>&nbsp;Cancelar</a>&nbsp;
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Confirmar</button>
    </div>
    </form>
@endsection
