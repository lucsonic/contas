@extends('templates.template_padrao.master')
@section('title', 'Despesas')

@section('content')
    <div style="margin-bottom: 20px; margin-top: 10px; font-weight: bold;">
        <h4><i class="fas fa-credit-card"></i>&nbsp;Despesas do Cartão</h4>
    </div>
    @if(session('mensagem'))
    	<div class="alert alert-success auto-fechar text-center" id="success-alert">
            <i class="fa-solid fa-circle-check" style="width: 24px;"></i>&nbsp;&nbsp;{{session('mensagem')}}
        </div>
    @endif
    <div style="margin-bottom: 10px;">
        <a class="btn btn-secondary" href="{{ route('cadastroDespesa') }}"><i class="fas fa-credit-card"></i>&nbsp;Nova Despesa</a>
    </div>
    <form action="{{ route('pesquisaPeriodo') }}" method="POST">
        @csrf
        <div style="text-align: left; display: flex; margin-top: 10px; margin-bottom: 10px;">
            <div class="form-group col-sm-2">
                <label for="dat_inicial" class="fw-bold">DATA INICIAL</label>
                <input type="text" value="" OnKeyPress="formatar(this, '##/##/####')" maxlength="10" required class="form-control" id="dat_inicial" name="dat_inicial" placeholder="Data inicial">
            </div>
            &nbsp;&nbsp;&nbsp;
            <div class="form-group col-sm-2">
                <label for="dat_final" class="fw-bold">DATA FINAL</label>
                <input type="text" value="" OnKeyPress="formatar(this, '##/##/####')" maxlength="10" required class="form-control" id="dat_final" name="dat_final" placeholder="Data final">
            </div>
            <div class="col-sm-2" style="padding-left: 10px; padding-top: 23px;">
                @if(!isset($pesquisa))
                    <button type="submit" id="btnPesquisar" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Pesquisar</button>
                @endif
                @if(isset($pesquisa))
                    <a type="buttom" href="{{ route('despesa') }}" id="btnVoltar" class="btn btn-secondary"><i class="fa fa-rotate"></i>&nbsp;Atualizar</a>
                @endif
            </div>
        </div>
    </form>
    @if(isset($pesquisa))
        <div style="margin-bottom: 5px;">
            <label class="fw-bold">{{$pesquisa}}</label>
        </div>
    @endif
    <div class="table-responsive">
    <table id="tblDados" class="table table-striped table-bordered table-hover" style="width:100%">
    <thead class="table-secondary">
        <tr>
            <th class="text-center" style="width: 10%">CARTÃO</th>
            <th class="text-center" style="width: 10%">DATA COMPRA</th>
            <th class="text-left" style="width: 30%">DESCRIÇÃO DA COMPRA</th>
            <th class="text-center" style="width: 10%">PARCELA</th>
            <th class="text-left" style="width: 20%">ESTABELECIMENTO</th>
            <th class="text-end" style="width: 10%">VALOR</th>
            <th class="text-center" style="width: 10%">AÇÕES</th>
        </tr>
    </thead>
        <tbody>
            @foreach ($despesas as $despesa)
                <tr>
                    <td class="text-center" style="vertical-align: middle;">{{$despesa->dsc_bandeira}}</td>
                    <td class="text-center" style="vertical-align: middle;">{{dataFrontPHP($despesa->dat_despesa)}}</td>
                    <td class="text-left" style="vertical-align: middle;">{{$despesa->dsc_despesa}}</td>
                    <td class="text-center" style="vertical-align: middle;">{{$despesa->num_parcela  . ' de ' . $despesa->tot_parcelas}}</td>
                    <td class="text-left" style="vertical-align: middle;">{{$despesa->dsc_estabelecimento}}</td>
                    <td class="text-end fw-bolder" style="vertical-align: middle;">{{valorFrontend($despesa->vlr_despesa)}}</td>
                    <td class="text-center" style="vertical-align: middle;">
                        <a type="button" href="{{ route('editarDespesa', ['codDespesa' => $despesa->cod_despesa]) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                        <a type="button" href="{{ route('excluirDespesa', ['codDespesa' => $despesa->cod_despesa]) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Excluir" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </div>
<script>
$(document).ready(function() {
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
        $("#success-alert").slideUp(500);
    });
});
</script>
@endsection
