@extends('templates.template_padrao.master')
@section('title', 'Cadastrar Conta a Pagar')

@section('content')
    <div style="margin-bottom: 20px; margin-top: 10px; font-weight: bold;">
        <h4><i class="fas fa-file-invoice-dollar"></i>&nbsp;Cadastro de Conta a Pagar</h4>
    </div>
    <form action="{{ isset($conta->cod_contasp) ? '/alterar-conta' : '/cadastrar-conta' }}" method="post">
    @csrf
    <div class="card" id="divCadastroContaPagar" style="background-color: #f1f1f1;">
        <div class="card-body">
            <div class="row" style="margin-bottom: 15px;">
                <input type="hidden" name="cod_contasp" value="{{$conta->cod_contasp ?? ''}}">
                <div class="form-group col-sm-2">
                    <label for="nom_mes" class="fw-bold">MÊS</label>
                    <select class="form-select" id="nom_mes" name="nom_mes" required>
                        <option value="{{$conta->nom_mes ?? ''}}">{{$conta->nom_mes ?? 'Selecione'}}</option>
                        <option value="Janeiro">Janeiro</option>
                        <option value="Fevereiro">Fevereiro</option>
                        <option value="Março">Março</option>
                        <option value="Abril">Abril</option>
                        <option value="Maio">Maio</option>
                        <option value="Junho">Junho</option>
                        <option value="Julho">Julho</option>
                        <option value="Agosto">Agosto</option>
                        <option value="Setembro">Setembro</option>
                        <option value="Outubro">Outubro</option>
                        <option value="Novembro">Novembro</option>
                        <option value="Dezembro">Dezembro</option>
                    </select>
                </div>
                <div class="form-group col-sm-1">
                    <label for="num_ano" class="fw-bold">ANO</label>
                    <input type="text" value="{{$conta->num_ano ?? ''}}" onkeypress="return somenteNumeros(event)" maxlength="4" required class="form-control" id="num_ano" name="num_ano" placeholder="Ano">
                </div>
                <div class="form-group col-sm-7">
                    <label for="dsc_contap" class="fw-bold">DESCRIÇÃO</label>
                    <input type="text" value="{{$conta->dsc_contap ?? ''}}" required class="form-control" id="dsc_contap" name="dsc_contap" placeholder="Descrição">
                </div>
                <div class="form-group col-sm-2">
                    <label for="vlr_contap" class="fw-bold">VALOR</label>
                    <input type="text" value="{{isset($conta->vlr_contap) ? valorFrontend($conta->vlr_contap) : ''}}" onkeypress="mascara(this, mvalor);" maxlength="14" required class="form-control" id="vlr_contap" name="vlr_contap" placeholder="Valor compra">
                </div>
            </div>
        </div>
    </div>
    <div style="text-align: right; margin-top: 20px;">
        <a type="buttom" href="{{ route('contas') }}" class="btn btn-secondary"><i class="fa fa-ban"></i>&nbsp;Cancelar</a>&nbsp;
        <button type="submit" class="btn btn-primary"><i class="fa fa-file-arrow-up"></i>&nbsp;Salvar</button>
    </div>
    </form>
@endsection
