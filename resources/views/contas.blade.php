@extends('templates.template_padrao.master')
@section('title', 'Contas a pagar')

@section('content')
    <div style="margin-bottom: 20px; margin-top: 10px; font-weight: bold;">
        <h4><i class="fas fa-file-invoice-dollar"></i>&nbsp;Contas a Pagar</h4>
    </div>
    <?php
        $pago = 0;
        $apagar = 0;
    ?>
    @if(session('mensagem'))
    	<div class="alert alert-success auto-fechar text-center" id="success-alert">
            <i class="fa-solid fa-circle-check" style="width: 24px;"></i>&nbsp;&nbsp;{{session('mensagem')}}
        </div>
    @endif
    <div style="margin-bottom: 10px;">
        <a class="btn btn-secondary" href="{{ route('cadastroContap') }}"><i class="fas fa-file-invoice-dollar"></i>&nbsp;Novo Item</a>
    </div>
    <form action="{{ route('pesquisaContasMes') }}" method="POST">
        @csrf
        <div style="text-align: left; display: flex; margin-top: 10px; margin-bottom: 10px;">
            <div class="form-group col-sm-2">
            <label for="mes" class="fw-bold">MÊS</label>
                <select class="form-select" id="mes" name="mes" required>
                    <option value="">Selecione</option>
                    <option value="Janeiro">Janeiro</option>
                    <option value="Fevereiro">Fevereiro</option>
                    <option value="Março">Março</option>
                    <option value="Abril">Abril</option>
                    <option value="Maio">Maio</option>
                    <option value="Junho">Junho</option>
                    <option value="Julho">Julho</option>
                    <option value="Agosto">Agosto</option>
                    <option value="Setembro">Setembro</option>
                    <option value="Outubro">Outubro</option>
                    <option value="Novembro">Novembro</option>
                    <option value="Dezembro">Dezembro</option>
                </select>
            </div>
            &nbsp;&nbsp;&nbsp;
            <div class="form-group col-sm-1">
                <label for="ano" class="fw-bold">ANO</label>
                <input type="text" value="" onkeypress="return somenteNumeros(event)" maxlength="4" required class="form-control" id="ano" name="ano" placeholder="Ano">
            </div>
            <div class="col-sm-2" style="padding-left: 10px; padding-top: 23px;">
                <button type="submit" id="btnPesquisar" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Pesquisar</button>
            </div>
        </div>
    </form>
    @if(isset($pesquisa))
        <div style="margin-bottom: 5px;">
            <label class="fw-bold">{{$pesquisa}}</label>
        </div>
    @endif
    <div class="table-responsive">
    <table id="tblDados" class="table table-striped table-bordered table-hover" style="width:100%">
    <thead class="table-secondary">
        <tr>
            <th class="text-center" style="width: 10%">MÊS</th>
            <th class="text-center" style="width: 10%">ANO</th>
            <th class="text-left" style="width: 50%">DESCRIÇÃO DO ITEM</th>
            <th class="text-center" style="width: 10%">PAGO</th>
            <th class="text-end" style="width: 10%">VALOR</th>
            <th class="text-center" style="width: 10%">AÇÕES</th>
        </tr>
    </thead>
        <tbody>
            @foreach ($contas as $conta)
            @if($conta->flg_pago === '0')
                <?php $apagar = $apagar + $conta->vlr_contap ?>
            @else
                <?php $pago = $pago + $conta->vlr_contap ?>
            @endif
                <tr>
                    <td class="text-center" style="vertical-align: middle;">{{$conta->nom_mes}}</td>
                    <td class="text-center" style="vertical-align: middle;">{{$conta->num_ano}}</td>
                    <td class="text-left fw-bolder" style="vertical-align: middle;">{{$conta->dsc_contap}}</td>
                    <td class="text-center" style="vertical-align: middle;">
                        @if($conta->flg_pago === '1')
                            <span class="badge bg-success">Sim</span>
                        @else
                            <span class="badge bg-danger">Não</span>
                        @endif
                    </td>
                    <td class="text-end" style="vertical-align: middle;">{{valorFrontend($conta->vlr_contap)}}</td>
                    <td class="text-center" style="vertical-align: middle;">
                        @if($conta->flg_pago === '0')
                            <a type="button" href="{{ route('pagarContap', ['codContasp' => $conta->cod_contasp]) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="A pagar" class="btn btn-warning btn-sm"><i class="fa fa-xmark"></i></a>
                        @else
                            <a type="button" href="{{ route('pagarContap', ['codContasp' => $conta->cod_contasp]) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Pago" class="btn btn-success btn-sm"><i class="fa fa-check"></i></a>
                        @endif
                        <a type="button" href="{{ route('editarContap', ['codContasp' => $conta->cod_contasp]) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                        <a type="button" href="{{ route('excluirContap', ['codContasp' => $conta->cod_contasp]) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Excluir" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div style="padding-top: 10px; text-align: center;">
        <table>
            <tr>
                <td style="font-size: 22px; text-align:right; font-weight: bold;">Total pago&nbsp;<i class="fa-solid fa-right-long"></i></td>
                <td style="color: green; font-size: 22px; font-weight: bold; text-align:right; padding-left: 10px;">{{valorFrontend($pago)}}</td>
            </tr>
            <tr>
                <td style="font-size: 22px; text-align:right; font-weight: bold;">Total a pagar&nbsp;<i class="fa-solid fa-right-long"></i></td>
                <td style="color: red; font-size: 22px; font-weight: bold; text-align:right; padding-left: 10px;">{{valorFrontend($apagar)}}</td>
            </tr>
        </table>
    </div>
    </div>
<script>
$(document).ready(function() {
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
        $("#success-alert").slideUp(500);
    });
});
</script>
@endsection
