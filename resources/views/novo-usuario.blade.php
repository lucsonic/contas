@extends('templates.template_padrao.master')
@section('title', 'Novo Usuário')

@section('content')
    <div style="margin-bottom: 20px; margin-top: 10px; font-weight: bold;">
        <h4><i class="fas fa-user-plus"></i>&nbsp;Cadastrar Usuário</h4>
    </div>
    <form action="{{ route('cadastrarUsuario') }}" method="post">
    @csrf
    <div class="card" id="divCadastroUsuario" style="background-color: #f1f1f1;">
        <div class="card-body">
            <div class="row" style="margin-bottom: 15px;">
                <div class="form-group col-sm-4">
                    <label for="nom_usuario" class="fw-bold">NOME</label>
                    <input type="text" value="" required class="form-control" id="nom_usuario" name="nom_usuario" placeholder="Nome">
                </div>
                <div class="form-group col-sm-4">
                    <label for="dsc_email" class="fw-bold">EMAIL</label>
                    <input type="text" value="" required class="form-control" id="dsc_email" name="dsc_email" placeholder="Email">
                </div>
                <div class="form-group col-sm-4">
                    <label for="dsc_senha" class="fw-bold">SENHA</label>
                    <input type="password" value="" required class="form-control" id="dsc_senha" name="dsc_senha" placeholder="Senha">
                </div>
            </div>
        </div>
    </div>
    <div style="text-align: right; margin-top: 20px;">
        <a type="buttom" href="{{ route('home') }}" class="btn btn-secondary"><i class="fa fa-ban"></i>&nbsp;Cancelar</a>&nbsp;
        <button type="submit" class="btn btn-primary"><i class="fa fa-file-arrow-up"></i>&nbsp;Salvar</button>
    </div>
    </form>
@endsection
