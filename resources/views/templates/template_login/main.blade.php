<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="{{ asset('assets/js/jquery-3.5.1.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}">
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>

    <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon.ico') }}">
    <title>CONT@S - @yield('title')</title>
</head>

<body id="bodi">
    <div class="app container-fluid" id="conteudo">
        @yield('content')
    </div>
</body>

</html>

<style>
    #conteudo {
        padding-top: 15px;
    }
    #bodi {
    background-image: url("{{ asset('assets/img/fundo.jpg') }}");
  }
</style>