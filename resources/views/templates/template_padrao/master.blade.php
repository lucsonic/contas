<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="{{ asset('assets/js/jquery-3.5.1.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('assets/fontawesome/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/datatables/css/dataTables.bootstrap5.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}">
  <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/bootstrap/js/popper.min.js') }}"></script>
  <script src="{{ asset('assets/datatables/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/datatables/js/dataTables.bootstrap5.min.js') }}"></script>
  <script src="{{ asset('assets/js/helpers.js') }}"></script>
  @include('sweetalert::alert')

  <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}">
  <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>

  <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon.ico') }}">
  <title>CONT@S - @yield('title')</title>
</head>

<body id="bodi">
  {{-- Barra superior --}}
  <nav class="navbar" style="background-color: navy">
    <div>
      <ul class="nav">
        {{-- <li class="nav-item">
          <a class="nav-link active text-light" aria-current="page" href="{{ url('/home') }}"><i class="fas fa-home"></i>&nbsp;Home</a>
        </li> --}}
        <li class="nav-item">
            <a class="nav-link text-light" aria-current="page" href="{{ url('/despesa') }}"><i class="fas fa-credit-card"></i>&nbsp;Cartão de Crédito</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-light" href="{{ url('/contas') }}"><i class="fas fa-file-invoice-dollar"></i>&nbsp;Contas a Pagar</a>
        </li>
      </ul>
    </div>
    <li class="nav-item dropdown d-flex text-light" style="float: right; padding-right: 30px;">
      <a class="nav-link dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        <i class="fas fa-user"></i>&nbsp;&nbsp;Olá, <?= session('nome')?>
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
        @if(session('perfil') === 1)
          <li><a class="dropdown-item" href="{{ url('novo-usuario') }}"><i class="fas fa-user-plus"></i>&nbsp;Novo Usuário</a></li>
        @endif
        <li><a class="dropdown-item" href="{{ url('troca-senha') }}"><i class="fas fa-key"></i>&nbsp;Trocar Senha</a></li>
        <li><a class="dropdown-item" href="{{ url('logout') }}"><i class="fas fa-right-from-bracket"></i>&nbsp;Sair</a></li>
      </ul>
    </li>
  </nav>

  {{-- Conteúdo --}}
  <div class="app container-fluid" style="margin-bottom: 70px;" id="conteudo">
      @yield('content')
  </div>

  {{-- Barra inferior --}}
  <div>
    <footer class="fixed-bottom text-white text-center" style="padding-bottom: 5px; padding-top: 5px; background-color: navy;">
      <div id="footer-page" style="display: flex;">
        <div style="width: 33%; text-align: left; margin-left: 10px;">CONT@S - Controle Financeiro</div>
        <div style="width: 34%; text-align: center;">Develop by lucsonic@gmail.com</div>
        <div style="width: 33%; text-align: right; margin-right: 10px;"><?= 'v. ' . substr(getVersao(), 1)?></div>
      </div>
    </footer>
  </div>
</body>

</html>

<style>
  #footer-page {
    font-size: 12px;
  }
  #conteudo {
    padding-top: 15px;
  }
  #bodi {
    background-image: url("{{ asset('assets/img/fundo.jpg') }}");
  }
</style>