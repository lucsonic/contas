@extends('templates.template_padrao.master')
@section('title', 'Cadastrar Despesa')

@section('content')
    <div style="margin-bottom: 20px; margin-top: 10px; font-weight: bold;">
        <h4><i class="fas fa-credit-card"></i>&nbsp;Cadastro de Compra com Cartão</h4>
    </div>
    <form action="{{ isset($despesa->cod_despesa) ? '/alterar-despesa' : '/cadastrar-despesa' }}" method="post">
    @csrf
    <div class="card" id="divCadastroDespesa" style="background-color: #f1f1f1;">
        <div class="card-body">
            <div class="row" style="margin-bottom: 15px;">
                <input type="hidden" name="cod_despesa" value="{{$despesa->cod_despesa ?? ''}}">
                <input type="hidden" name="estabelecimento" value="{{isset($desp) ? $desp->dsc_estabelecimento : ''}}">
                <input type="hidden" name="descricao" value="{{isset($desp) ? $desp->dsc_despesa : ''}}">
                <input type="hidden" name="valor" value="{{isset($desp) ? $desp->vlr_despesa : ''}}">
                <div class="form-group col-sm-2">
                    <label for="cod_cartao" class="fw-bold">CARTÃO</label>
                    <select class="form-select" id="cod_cartao" name="cod_cartao" required>
                        <option value="{{$despesa->cod_cartao ?? ''}}">{{$despesa->dsc_bandeira ?? 'Selecione'}}</option>
                        @foreach($cartoes as $cartao)
                            <option value="{{$cartao['cod_cartao']}}"> {{ $cartao['dsc_bandeira'] }} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-sm-2">
                    <label for="dat_despesa" class="fw-bold">DATA COMPRA</label>
                    <input type="text" value="{{isset($despesa->dat_despesa) ? dataFrontPHP($despesa->dat_despesa) : ''}}" OnKeyPress="formatar(this, '##/##/####')" maxlength="10" required class="form-control" id="dat_despesa" name="dat_despesa" placeholder="Data compra">
                </div>
                <div class="form-group col-sm-6">
                    <label for="dsc_despesa" class="fw-bold">DESCRIÇÃO DA COMPRA</label>
                    <input type="text" value="{{$despesa->dsc_despesa ?? ''}}" required class="form-control" id="dsc_despesa" name="dsc_despesa" placeholder="Descrição da compra">
                </div>
                <div class="form-group col-sm-2">
                    <label for="tot_parcelas" class="fw-bold">N° PARCELAS</label>
                    @if(isset($despesa))
                        <input type="text" value="{{$despesa->tot_parcelas ?? ''}}" required class="form-control" id="tot_parcelas" name="tot_parcelas" readonly placeholder="N° de parcelas">
                    @else
                        <input type="text" value="{{$despesa->tot_parcelas ?? ''}}" required class="form-control" id="tot_parcelas" name="tot_parcelas" onkeypress="return somenteNumeros(event)" placeholder="N° de parcelas">
                    @endif
                </div>
            </div>
            <div class="row" style="margin-bottom: 15px;">
                <div class="form-group col-sm-6">
                    <label for="dsc_estabelecimento" class="fw-bold">NOME ESTABELECIMENTO</label>
                    <input type="text" value="{{$despesa->dsc_estabelecimento ?? ''}}" required class="form-control" id="dsc_estabelecimento" name="dsc_estabelecimento" placeholder="Nome do estabelecimento">
                </div>
                <div class="form-group col-sm-2">
                    <label for="vlr_despesa" class="fw-bold">VALOR</label>
                    <input type="text" value="{{isset($despesa->vlr_despesa) ? valorFrontend($despesa->vlr_despesa) : ''}}" onkeypress="mascara(this, mvalor);" maxlength="14" required class="form-control" id="vlr_despesa" name="vlr_despesa" placeholder="Valor compra">
                </div>
            </div>
        </div>
    </div>
    <div style="text-align: right; margin-top: 20px;">
        <a type="buttom" href="{{ route('despesa') }}" class="btn btn-secondary"><i class="fa fa-ban"></i>&nbsp;Cancelar</a>&nbsp;
        <button type="submit" class="btn btn-primary"><i class="fa fa-file-arrow-up"></i>&nbsp;Salvar</button>
    </div>
    </form>
@endsection
